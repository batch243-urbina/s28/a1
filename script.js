// SESSION 28 ACTIVITY 1 SOLUTIONS

// Insert Single Room

db.rooms.insert({
  name: "single",
  accommodation: "2",
  price: 1000,
  description: "A simple room with all the basic necessities",
  roomsAvailable: 10,
  isAvailable: false,
});

// Insert Multiple Rooms

db.rooms.insertMany([
  {
    name: "double",
    accommodation: "3",
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    roomsAvailable: 5,
    isAvailable: false,
  },
  {
    name: "queen",
    accommodation: "4",
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    roomsAvailable: 15,
    isAvailable: false,
  },
]);

// Search for a room with the name double

db.rooms.find({ name: "double" });

// Update the queen room and set the available rooms to 0

db.rooms.updateOne({ name: "queen" }, { $set: { roomsAvailable: 0 } });

// Delete rooms with 0 availability

db.rooms.deleteMany({ roomsAvailable: 0 });
